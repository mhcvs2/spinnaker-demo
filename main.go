package main

import (
	_ "spinnaker-demo/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

