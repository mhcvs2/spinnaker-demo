package routers

import (
	"spinnaker-demo/controllers"
	"github.com/astaxie/beego"
	"spinnaker-demo/controllers/docker"
)

func init() {
    beego.Router("/", &controllers.MainController{})
	ns := beego.NewNamespace("/docker",
		beego.NSInclude(
			&docker.DController{},
		))
	beego.AddNamespace(ns)
}
