#!/usr/bin/env bash

image=mhcvs2/spinnakerdemo
tag=$(date +"%y%m%d-%H%M%S")

docker login -umhcvs2 -pqq77aa88

echo "build image ${image}:${tag}..."
docker build -t ${image}:${tag} .

echo "push image ${image}:${tag}..."
docker push ${image}:${tag}