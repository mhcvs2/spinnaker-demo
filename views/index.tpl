<!DOCTYPE html>

<html>
<head>
  <title>Spinnaker Demo</title>
  <meta http-equiv="refresh" content="300">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="show">
        <h class="name">Spinnaker demo</h>
        <p class="plat">Platform: Ceres 1.5.0</p>
        <p class="plat">Spinnaker version: 1.7.0</p>
        <p class="main">Application version: 1.0.0</p>
    </div>

</body>
</html>

<style>
    .show {
        text-align: center;
        border: solid;
        margin-top: 100px;
        margin-left: 300px;
        margin-right: 300px;
        padding-top: 30px;
    }
    .name {
        font-size: 80px;
    }
    .plat {
        font-size: 40px;
    }
    .main {
        font-size: 60px;
        color: red;
    }
</style>
